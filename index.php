<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S5 Activity</title>
</head>
<body>
	<?php session_start(); ?>

	<h1>Login Page</h1>
	<form method="POST" action="./server.php">
		Username: <input type="email" name="email" required>
		Password: <input type="password" name="password" required>
		<button type="submit">Login</button>
	</form>

	<?php if(isset($_SESSION['email'])): ?>

		<p>Welcome, <?= $_SESSION['email']; ?></p>

	<?php else: ?>

		<?php if(isset($_SESSION['error'])): ?>

			<p><?= $_SESSION['error']; ?></p>

		<?php endif;?>	

	<?php endif;?>
</body>
</html>
